package fall2023ee382c16.pset3;

import static org.junit.Assert.*;
import java.util.TreeSet;
import java.util.Set;

import org.junit.Test;

public class GraphTester {
    // tests for method "addEdge" in class "Graph"
    @Test public void testAddEdge0() {
        Graph g = new Graph(2);
        g.addEdge(0, 1);
        System.out.println(g);
        assertEquals(g.toString(), "numNodes: 2\nedges: [[false, true], [false, false]]");
    }

    // your tests for method "addEdge" in class "Graph" go here

    // provide at least 4 test methods such that together they provide full statement
    //   coverage of your implementation of addEdge and any helper methods;
    // each test method has at least 1 invocation of addEdge;
    // each test method creates exactly 1 graph
    // each test method creates a unique graph w.r.t. "equals" method
    // each test method has at least 1 test assertion;
    // each test assertion correctly characterizes expected behavior with respect to the spec;

    // ...
    //Test trying to add an edge from an illegal node (negative)
    @Test (expected=IllegalArgumentException.class)
    public void testAddingEdgeLessThan0() {
        Graph g = new Graph(3);
        g.addEdge(0, 1);
        g.addEdge(1, 2);
        g.addEdge(-1, 1);
        System.out.println(g);
        assertEquals(g.toString(), "numNodes: 3\nedges: [[false, true, false], [false, false, true], [false, false, false]]");
    }

    //Test trying to add an edge from an illegal node (>= numNodes)
    @Test (expected=IllegalArgumentException.class)
    public void testAddingEdgeGreaterThannumNodes() {
        Graph g = new Graph(2);
        g.addEdge(0, 0);
        g.addEdge(1, 2);
        System.out.println(g);
        assertEquals(g.toString(), "numNodes: 2\nedges: [[true, false], [false, false]]");
    }

    //Test trying to add two illegal edges from an illegal node (negative & >= numNodes)
    @Test (expected=IllegalArgumentException.class)
    public void testAddingEdgeLessThan0AndGreaterThannumNodes() {
        Graph g = new Graph(4);
        g.addEdge(0, 3);
        g.addEdge(-1, 2);
        g.addEdge(2, 0);
        g.addEdge(3, 4);
        System.out.println(g);
        assertEquals(g.toString(), "numNodes: 4\nedges: [[false, false, false, true], [false, false, false, false], [true, false, false, false], [false, false, false, false]]");
    }

    //Test adding valid edges
    @Test 
    public void testValidAddEdge() {
        Graph g = new Graph(3);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 0);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 1);
        System.out.println(g);
        assertEquals(g.toString(), "numNodes: 3\nedges: [[false, true, true], [true, false, true], [true, true, false]]");

    }

    // tests for method "reachable" in class "Graph"

    @Test public void testReachable0() {
        Graph g = new Graph(1);
        Set<Integer> nodes = new TreeSet<Integer>();
        nodes.add(0);
        assertTrue(g.reachable(nodes, nodes));
    }

    // your tests for method "reachable" in class "Graph" go here

    // provide at least 6 test methods such that together they provide full statement
    //   coverage of your implementation of reachable and any helper methods;
    // each test method has at least 1 invocation of reachable;
    // each test method has at least 1 test assertion;
    // at least 2 test methods have at least 1 invocation of addEdge;

    // ...
    //This test provides a sources list with an illegal node. The test
    //should see a IllegalArgumentException
    @Test (expected=IllegalArgumentException.class)
    public void testSourcesContainsIllegalNode() {
        Graph g = new Graph(5);
        Set<Integer> sources = new TreeSet<>();
        sources.add(-1);
        Set<Integer> targets = new TreeSet<>();
        targets.add(2);
        assertTrue(g.reachable(sources, targets));
    }

    //This test provides a targets list with an illegal node. The test
    //should see a IllegalArgumentException
    @Test (expected=IllegalArgumentException.class)
    public void testTargetsContainsIllegalNode() {
        Graph g = new Graph(5);
        Set<Integer> sources = new TreeSet<>();
        sources.add(3);
        Set<Integer> targets = new TreeSet<>();
        targets.add(15);
        assertTrue(g.reachable(sources, targets));
    }

    //This test tries a scenario where some targets have a path 
    //from the sources but one target does not
    @Test 
    public void testOneTargetHasNoPath() {
        Graph g = new Graph(3);
        g.addEdge(0, 1);
        g.addEdge(1, 1);
        g.addEdge(1, 2);
        g.addEdge(2, 1);

        Set<Integer> sources = new TreeSet<>();
        sources.add(1);
        sources.add(2);
        Set<Integer> targets = new TreeSet<>();
        targets.add(0);
        targets.add(1);
        targets.add(2);

        assertFalse(g.reachable(sources, targets));

    }

    //This test tries a scenario where all targets have no path
    //from the sources
    @Test
    public void testAllTargetsHaveNoPath() {
        Graph g = new Graph(4);
        g.addEdge(0, 0);
        g.addEdge(0, 1);
        g.addEdge(2, 2);
        g.addEdge(2, 3);

        Set<Integer> sources = new TreeSet<>();
        sources.add(1);
        sources.add(3);
        Set<Integer> targets = new TreeSet<>();
        targets.add(0);
        targets.add(2);

        assertFalse(g.reachable(sources, targets));
    }

    //This test tries a scenario where all targets have a direct path.
    //i.e. all targets have a source where edges[source][target] == true
    @Test
    public void testAllTargetsHaveDirectPaths() {
        Graph g = new Graph(4);
        g.addEdge(0, 1);
        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(3, 0);

        Set<Integer> sources = new TreeSet<>();
        sources.add(0);
        sources.add(1);
        sources.add(2);
        sources.add(3);
        Set<Integer> targets = new TreeSet<>();
        targets.add(0);
        targets.add(1);
        targets.add(2);
        targets.add(3);

        assertTrue(g.reachable(sources, targets));
    }

    //This test tries a scenario where all targets have an indirect path
    //from sources. I.e. all targets have a path that originates from some
    //source but goes through a node not in the sources list before
    //reaching the target node.
    @Test
    public void testAllTargetsHaveIndirectPaths() {
        Graph g = new Graph(6);
        g.addEdge(0, 1);
        g.addEdge(2, 3);
        g.addEdge(1, 4);
        g.addEdge(3, 5);

        Set<Integer> sources = new TreeSet<>();
        sources.add(0);
        sources.add(2);
        Set<Integer> targets = new TreeSet<>();
        targets.add(4);
        targets.add(5);

        assertTrue(g.reachable(sources, targets));
    }
}
