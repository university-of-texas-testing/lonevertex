package fall2023ee382c16.pset3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Set;

public class Graph {
    private int numNodes; // number of nodes in the graph
    private boolean[][] edges;
    // edges[i][j] is true if and only if there is an edge from node i to node j

    // class invariant: edges != null; edges is a square matrix;
    //                  numNodes >= 0; numNodes is number of rows in edges

    public Graph(int size) {
        numNodes = size;

        // your code goes here
        // ...

        //If numNodes is negative, throw exception. Can't have a graph with negative number of nodes.
        if (numNodes < 0) {
            throw new IllegalArgumentException();
        } else {
            // initialize a square matrix of size numNodes
            edges = new boolean[numNodes][numNodes];

            //initialize edges to all false since no edges are added at this point
            for (int x = 0; x < numNodes; x++) {
                for (int y = 0; y < numNodes; y++) {
                    edges[x][y] = false;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "numNodes: " + numNodes + "\n" + "edges: " + Arrays.deepToString(edges);
    }
    
    @Override
    public boolean equals(Object o) {
        if (o.getClass() != fall2023ee382c16.pset3.Graph.class) return false;
        return toString().equals(o.toString());
    }

    public void addEdge(int from, int to) {
        // postcondition: adds a directed edge "from" -> "to" to this graph

        // your code goes here
        // ...
        if (from < 0 || to < 0) {
            throw new IllegalArgumentException();
        } else if (from >= numNodes || to >= numNodes ) {
            throw new IllegalArgumentException();
        } else {
            edges[from][to] = true;
        }

    }

    public boolean reachable(Set<Integer> sources, Set<Integer> targets) {
        if (sources == null || targets == null) throw new IllegalArgumentException();

        // postcondition: returns true if (1) "sources" does not contain an illegal node,
        //                        (2) "targets" does not contain an illegal node, and
        //                        (3) for each node "m" in set "targets", there is some
        //                        node "n" in set "sources" such that there is a directed
        //                        path that starts at "n" and ends at "m" in "this"; and
        //                        false otherwise


        // your code goes here
        // ...

        //make sure both sets have only valid values
        for (Integer source: sources) {
            if (source < 0 || source >= numNodes)
                throw new IllegalArgumentException();
        }

        for (Integer target: targets) {
            if (target < 0 || target >= numNodes)
                throw new IllegalArgumentException();
        }

        //for every value in targets, find all the nodes that lead to it and add them to
        //the visited arraylist
        for (int m: targets) {
            PriorityQueue<Integer> nodes2Check = new PriorityQueue<>();
            ArrayList<Integer> visited = new ArrayList<>();

            //initialize the queue of nodes2Check as well as visited to all
            //nodes that have a direct path to target m
            for (int n = 0; n < numNodes; n++) {
                if (edges[n][m]) {
                    nodes2Check.add(n);
                    visited.add(n);
                }
            }

            //while there are more nodes to check, add them to list of ones to check
            //as well as keep track of which nodes you have already visited
            while (!nodes2Check.isEmpty()) {
                int source = nodes2Check.remove();
                for (int x = 0; x < numNodes; x++) {
                    if (edges[x][source]) {
                        if (!visited.contains(x)) {
                            nodes2Check.add(x);
                            visited.add(x);
                        }
                    }
                }
            }

            //Now go through sources and make sure at least one of them
            //is considered a node that has a path that leads to m
            boolean isMReachable = false;
            if (sources.contains(m)) {      //node is in targets and sources (it has  path from itself)
                continue;
            }
            for (int n: sources) {
                if (visited.contains(n)) {
                    isMReachable = true;
                    break;
                }
            }

            //if no path was found, skip checking the rest and return false
            if (!isMReachable) {
                return false;
            }
        }

        //all nodes had a direct path so return true
        return true;

    }
}
